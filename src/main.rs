use std::env;
use std::collections::HashMap;
use clap::{Arg, App, ArgMatches};
use trello::{
    Client,
    Board,
    List,
    Card,
    TrelloError,
};
extern crate config;

fn parse_args() -> ArgMatches<'static> {
    App::new("trello-todo")
        .author("Fam Zheng <fam@euphon.net>")
        .version("0.1.0")
        .about("Add a trello card to your board")
        .arg(Arg::with_name("BOARD")
            .help("Board name")
            .required(true)
            .index(1))
        .arg(Arg::with_name("LIST")
            .help("List name")
            .required(true)
            .index(2))
        .arg(Arg::with_name("ITEM")
            .help("List name")
            .required(false)
            .multiple(true)
            .index(3))
        .get_matches()
}

fn create_item(client: &Client, list_id: &str, name: &str) -> Result<Card, TrelloError> {
    let card = Card {
        id: String::from(""),
        name: String::from(name),
        desc: String::from(""),
        closed: false,
        url: String::from(""),
        labels: None,
    };
    Card::create(&client, &list_id, &card)
}

fn main() {
    let matches = parse_args();
    let mut settings = config::Config::default();
    let home = env::var("HOME").unwrap();
    let fname = home + "/.config/trello-todo";
    settings.merge(config::File::with_name(fname.as_str())).unwrap();
    let host = settings.get_str("host").unwrap();
    let token = settings.get_str("token").unwrap();
    let key = settings.get_str("key").unwrap();
    let client = Client {
        host: host,
        token: token,
        key: key,
    };
    let boards = Board::get_all(&client).unwrap();
    let boardname = matches.value_of("BOARD").unwrap();
    let listname = matches.value_of("LIST").unwrap();
    let mut list_id :Option<String> = None;
    let mut to_create = HashMap::new();
    let items: Vec<&str> = matches.values_of("ITEM").unwrap().collect();
    for item in items {
        to_create.insert(item, true);
    }
    for b in boards.iter() {
        if b.name == boardname.to_string() {
            let lists = List::get_all(&client, &b.id, true).unwrap();
            for l in lists {
                if l.name == listname.to_string() {
                    list_id = Some(l.id.clone());
                    if let Some(cards) = l.cards {
                        for c in cards {
                            to_create.remove(c.name.as_str());
                            println!("Existing: {}", c.name);
                        }
                    }
                }
            }
        }
    }
    match list_id {
        Some(id) => {
            for item in to_create.keys() {
                println!("Creating: {}", item);
                create_item(&client, &id, item).unwrap();
            }
        },
        None => {
            println!("Board/list not found");
        },
    }
}
